# Voter Registration Search
A simple JSON microservice to search voter registrations.
Built for the [Arlington County Office of Elections](https://vote.arlingtonva.us) to supplement the [Virginia Department of Elections](https://vote.elections.virginia.gov/VoterInformation) portal.

### Features

* Protects voter privacy (only the SHA-256 hash of PII is deployed)
* No database required
* Easily deployable to AWS Lambda and API Gateway

### Limitations

* Voters must provide their names (except for case) exactly as they appear in the roster
* Assumes the combination of first name, last name, and date of birth uniquely identifies each voter
* Must be redeployed to update the roster

## Installation
```bash
git clone https://bitbucket.org/ArlingtonCounty/voter-registration-search.git
cd voter-registration-search
virtualenv .
source bin/activate
pip install -r requirements.txt
```

## Processing Voter Roster
Create `voters.csv` in the format below:
```
LAST_NAME,FIRST_NAME,DOB,PRECINCT,PROTECTED,STATUS
DOE,JOHN,1970-12-31,001 Arlington,FALSE,Active
DOE,JANE,1970-12-31,001 Arlington,FALSE,Inactive
```

```bash
python parse_roster.py voters.csv
```

`voters.pkl` now contains the precinct code and status of each non-protected voter, keyed by the SHA-256 hash of their uppercased name and DOB.

## Running Locally
```bash
python app.py
```

The API is now running at http://localhost:5000. A sample search page is available at http://localhost:5000/static/search.html.

## API Requests
Parameters:

* `first`: required, case-insensitive
* `last`: required, case-insensitive
* `dob`: required, YYYY-MM-DD
* `callback`: optional, for JSONP

## API Responses
Responses have `Content-Type: application/json` by default. A successful match will return a JSON array with two elements:

* Precinct code (String)
* Status (Boolean) - True if active, false if inactive

If `callback` is provided, successful responses will have `Content-Type: application/javascript`.

### Match, active voter (HTTP 200)
```json
[
  "001", 
  true
]
```

### Match, inactive voter (HTTP 200)
```json
[
  "001", 
  false
]
```

### No match (HTTP 404)
```json
{
  "error": "Not Found"
}
```

### Missing URL parameter(s) (HTTP 400)
```json
{
  "error": "Bad Request"
}
```

## Deployment
We deploy to AWS [Lambda](https://aws.amazon.com/lambda/) and [API Gateway](https://aws.amazon.com/api-gateway/) with [Zappa](https://github.com/Miserlou/Zappa), and host the search page on S3.
The minimum Lambda memory size (128MB) is more than sufficient for a roster with 170,000 names.
Typical response times are less than 200ms.
One million searches (page load, API request, and Lambda invocation) will cost about $6, or less if your overall Lambda usage is within the free tier.

![flow.png](https://bitbucket.org/repo/xb4nBk/images/1206945900-flow.png)

![screenshot.png](https://bitbucket.org/repo/xb4nBk/images/3160061417-screenshot.png)

## License
GNU General Public License, Version 3 (see LICENSE).

## Copyright
2016 [Arlington County Government](https://www.arlingtonva.us).