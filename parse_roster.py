#!env/bin/python

"""Parse voter roster and output a pickled lookup table.

Usage: python parse_roster.py voters.csv

See README.md for the format of voters.csv.
"""

import sys
import csv
import hashlib
try:
    import cPickle as pickle
except:
    import pickle

__author__ = "Dylan Barlett"
__copyright__ = "Arlington County Government"
__license__ = "GPLv3"
__version__ = "0.1"
__maintainer__ = "Dylan Barlett"
__email__ = "dbarlett@arlingtonva.us"
__status__ = "Development"


if __name__ == '__main__':
    voters = {}
    with open(sys.argv[1], "rb") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row["PROTECTED"] == "FALSE":
                key = row["LAST_NAME"] + row["FIRST_NAME"] + row["DOB"]
                key = hashlib.sha256(key.upper()).hexdigest()
                precinct = row["PRECINCT"][0:3]
                if row["STATUS"] == "Active":
                    active = True
                else:
                    active = False
                voters[key] = (precinct, active)

    with open("voters.pkl", "wb") as outfile:
        pickle.dump(voters, outfile, 2)
