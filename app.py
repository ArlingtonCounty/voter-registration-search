#!env/bin/python

"""Voter registration search microservice.
"""

import hashlib
try:
    import cPickle as pickle
except:
    import pickle
from functools import wraps
from flask import current_app
from flask import Flask
from flask import abort
from flask import request
from flask import Response
from flask import jsonify

__author__ = "Dylan Barlett"
__copyright__ = "Arlington County Government"
__license__ = "GPLv3"
__version__ = "0.1"
__maintainer__ = "Dylan Barlett"
__email__ = "dbarlett@arlingtonva.us"
__status__ = "Development"

app = Flask(__name__)
with open("voters.pkl", "rb") as infile:
    VOTERS = pickle.load(infile)


def jsonp(func):
    """Wrap JSONified output for JSONP requests.
    From https://gist.github.com/1094140
    """
    @wraps(func)
    def decorated_function(*args, **kwargs):
        callback = request.args.get("callback", False)
        if callback:
            data = str(func(*args, **kwargs).data)
            content = str(callback) + "(" + data + ")"
            mimetype = "application/javascript"
            return current_app.response_class(content, mimetype=mimetype)
        else:
            return func(*args, **kwargs)
    return decorated_function


def voter_search(first, last, dob):
    """Search for a voter.
    """
    try:
        key = (last + first + dob).upper()
        key_hash = hashlib.sha256(key).hexdigest()
        return VOTERS[key_hash]
    except KeyError:
        return None


@app.errorhandler(400)
def bad_request(error=None):
    """Handle HTTP 400 errors.
    """
    resp = jsonify({"error": "Bad Request"})
    resp.status_code = 400
    return resp


@app.errorhandler(404)
def not_found(error=None):
    """Handle HTTP 404 errors.
    """
    resp = jsonify({"error": "Not Found"})
    resp.status_code = 404
    return resp


@app.route('/static/<path:filename>')
def static_content(filename):
    """Fallback for static content.
    """
    return app.send_from_directory("static", filename)


@app.route("/", methods=["GET"])
@jsonp
def index():
    """Index page.
    """
    required_params = [
        "first",
        "last",
        "dob",
    ]
    valid = {}
    for i in required_params:
        param_i = request.args.get(i, None)
        if param_i:
            valid[i] = param_i
        else:
            abort(400)

    voter = voter_search(
        valid["first"],
        valid["last"],
        valid["dob"]
    )
    if voter:
        return jsonify(voter)
    else:
        abort(404)


if __name__ == '__main__':
    app.run()
